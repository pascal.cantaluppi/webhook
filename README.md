# Gitlab Webhook

<p>
<img src="https://gitlab.com/pascal.cantaluppi/webhook/-/raw/master/public/img/webhooks.png" alt="Webhooks" />
</p>

Notification API for Webhooks triggered by Gitlab-Pipelines

<p>
<img src="https://gitlab.com/pascal.cantaluppi/webhook/-/raw/master/public/img/trigger.png" alt="Trigger" />
</p>

<p>
<img src="https://gitlab.com/pascal.cantaluppi/webhook/-/raw/master/public/img/banana.gif" width="50" height="50"alt="Banana" />
</p>
