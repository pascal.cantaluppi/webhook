var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", {
    title: "Notification API for Webhooks triggered by Gitlab-Pipelines",
  });
});

module.exports = router;
