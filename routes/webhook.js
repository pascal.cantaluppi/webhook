require("dotenv").config();

var express = require("express");
var router = express.Router();

var nodemailer = require("nodemailer");
var transporter = nodemailer.createTransport({
  host: "smtp.office365.com",
  port: 587,
  secure: false,
  auth: {
    user: "pascal.cantaluppi@ipso.ch",
    pass: Buffer.from(process.env.MAIL_SECRET, "base64").toString("utf-8"),
  },
  requireTLS: true,
  tls: {
    ciphers: "SSLv3",
  },
});
/* GET webhook */
router.get("/", (req, res, next) => {
  res.sendStatus(200);
});

/* POST webhook */
router.post("/", (req, res, next) => {
  if (req.header("X-Gitlab-Token") == process.env.GIT_TOKEN) {
    transporter.sendMail(
      {
        from: '"pascal.cantaluppi@ipso.ch"',
        to: "pascal.cantaluppi@ipso.ch",
        subject: "Notification from Gitlab-Pipeline",
        html:
          "This e-mail is a notification about a git commit into the master branch of the ipso website.<br /><br />Deployment trough ci/cd pipeline is now starting and taking the website temporary offline.<br /><br />This notification was triggered by a gitlab webhook using the ipso notification API.<br /><br />Date: " +
          new Date(),
      },
      function (error, response) {
        if (error) {
          console.log(error);
        }
      }
    ),
      res.sendStatus(200);
  } else {
    res.sendStatus(403);
  }
});

module.exports = router;
